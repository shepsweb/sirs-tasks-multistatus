# Sirs-Tasks-Multistatus #

Provides migrations, seeds, models, events, artisan commands, and jobs to support task management.

### Overview ###
Tasks are to-do items to be completed by or for their Owners (polymorphic relationship).  Tasks are described by their TaskType (Task belongsTo a TaskType) and their status is denoted by a TaskStatus (Task belongsTo a TaskStatus).

As of version 2.2.0 you can define a task router to route a user virtually anywhere when they go to the url of a task (see *tasks.router middleware* below).  Also as of 2.2.0, you can apply global scopes to tasks in config/tasks.php (see *Configuration* below).

### Versions ###
| Laravel   | Sirs\Tasks |
| -------   | ---------- |
| 10.0      | 10.0       |
| 9.0       | 9.0        |
| 6.0       | 6.0        |
| 5.7       | 4.0        |
| 5.5 - 5.6 | 3.x || 4.0 |
| 5.1 - 5.4 | 3.x        |
| 5.0       | 1.3.x      |  

### Installation ###
1. `composer require sirs/tasks-multistatus`
2. If you are not auto-detecting service providers add `Sirs\Tasks\TasksServiceProvider::class,` to config/app.php
3. Vendor Publish to get the config file, migrations, default seeders, and default TaskRouter.
4. Add your TaskTypeStatusSeeder to DatabaseSeeder.php and define your task types and statuses in database/seeds/type_status_definition.json (note that any attributes available on your TaskType or TaskStatus model can be defined here).
5. Any models that will be task owners must implement `Sirs\Tasks\Interfaces\Taskowner` which can be satisfied by using `Sirs\Tasks\TaskOwnerTraits`
6. Create EloquentResources for your TaskOwner models. (optional)

### Task Manager ###
The task manager is accessible at `/tasks/manager`. All HTML, CSS, and JavaScript is included and routed through the package.  If you need to customize the Task Manager you should copy the source files into your project and use/update it as you like.
```
cp -r vendor/sirs/tasks-multistatus/src/assets/js/components/Tasks resources/assets/js/components
```



**NOTE:** If you were previously using sirs/tasks (i.e. from the boilerplate), You'll probably want to completely remove all sirs/tasks stuff and vendor publish to start from scratch.

### Configuration ###
Customize TaskType and TaskStatus seeders to fit your project.  Then update the config as needed.  The config options are:

- **apiPrefix:** route prefix for the task api routes. Defaults to 'api'
- **routeGroup:** route group for task routes. defaults to ```['middleware' => ['auth','tasks.redirect']]```
- **owner_transformers:** map of ownerTypeClass=>ownerTypeTransformerClass where transformer class is a Fractal Transformer. Defaults to empty array;
- **global_scopes:** Array of global scopes to apply to the Task model. Defaults to empty array
- **taskToAction:** A map of TaskType->slug => action.  See config/tasks.php for examples
- **router:** Task router class name for use with tasks.router middleware. Defaults to published App\TaskRouter::class,
- **bindings.models:** use your own models that adhere to the relevant interfaces setting this. Example
```
...
    'bindings' => [
      'models' => [
        'Task' => \App\Task::class,             // where \App\Task implements \Sirs\Tasks\Interfaces\Task
        'TaskType' => \App\TaskType::class,     // where \App\Task implements \Sirs\Tasks\Interfaces\TaskType
        'TaskStatus' => \App\TaskStatus::class, // where \App\Task implements \Sirs\Tasks\Interfaces\TaskStatus
      ]
    ],
...
```

#### tasks.router middleware ####
Register the task.redirect middleware in App/Http/Kernel.php:
```
protected $middlewareAliases = [
...
        'tasks.redirect' => \Sirs\Tasks\Middleware\RedirectTasks::class,
]
```

Customize your taskActions map in config/tasks.php:
```
    'taskToAction'=>[
      /** examples:
       * 'task-slug' => 'action_type:slugOrIdentifier'
       * 'survey-task' => 'survey:surveySlug',
       * 'schedule-appt-task' => 'appointment:apptTypeId',
       * 'redirect-to-url' => 'url:/path/to/thing'
       * 'custom-task' => 'custome:someIdentifier'
       **/
    ],
```

TaskRouter class' resolve function in TaskRouter.php if you need custom task routing functionality:
```
  public function resolve(Task $task){
      $taskToAction = config('tasks.taskToAction');
      if( !isset($taskToAction[$task->taskType->slug]) ) return null;

      list($type, $identifier) = preg_split('/:/', $taskToAction[$task->taskType->slug]);

      switch ($type) {
        case 'survey':
          return url('app-participant/'.$task->owner->identifier.'/survey/'.$identifier);
        case 'appointment':
          $url = 'appointments/create?appointment_type_id='.$identifier.'&attendee_id='.$task->owner->identifier;
          switch ($identifier) {
            case 2:
              // $forTask = $task->owner->tasks->where('task_type_id', 1)->first();
              break;              
            default:
              $forTask = null;
              break;
          }
          $url = ( $forTask ) ? $url.'&task_id='.$forTask->identifier : $url;
          return url($url);
        case 'url':
          return url($identifier);
        default:
          return null;
          break;
      }
  }
```
This function should return a url for use in the Laravel redirect() helper function or null for default routing.

Even if you've mapped a task type to an action above you can bypass the router by adding `bypassRouter=1` to the tasks url.  For example I can force task 383 to go the default task form with the url: `/tasks/383?bypassRouter=1`

As of version 2.3.9 the middleware will pass query string attributes on the original task url.

Example task type of task 123 routes to /monkeys:
`http::project/tasks/123/food=fruit&drink=coffee` will redirect to `http::project/monkeys?food=fruit&drink=coffee`


### Task Events ###
Task lifecycle events are built on top of models events.

- Sirs\Tasks\Events\TaskStarted
- Sirs\Tasks\Events\TaskCompleted
- Sirs\Tasks\Events\TaskCanceled
- Sirs\Tasks\Events\TaskDataUpdated
- Sirs\Tasks\Events\TaskCreated
- Sirs\Tasks\Events\TaskStatusUpdated (deprecated)
- Sirs\Tasks\Events\TaskWasCreated (deprecated)
- Sirs\Tasks\Events\TaskStatusWasUpdated (deprecated)

#### TaskTypeWorkflowStrategy ####
To help consolidate task lifecycle logic you can now create workflow strategy classes that allow you to write event handling code for all events fired by the sirs/tasks package.  

TaskTypeWorkflowStrategy should live in app/Tasks and are constructed with the task and the event, and have methods corresponding to each of the TaskEvent types:
* TaskStarted -> TaskTypeWorkflowStrategy::started
* TaskDataUpdated - TaskTypeWorkflowStrategy::taskDataUpdated

Stubs for these files can be generated using the command:
```
php artisan tasks:workflow [task-type-slug|all]
```  
Using all in place of a task-type-slug will generate Workflow strategy classes for all task types.

TaskTypeWorkflowStrategies are optional and do not need to be created for a task type.

### Commands ###
* AutocancelTask - sets task status to task auto-canceled
* CancelTask - sets task status to canceled
* CompleteTask - sets task status to completed
* FailTask - sets task status to failed
* MissTask - sets task status to missed
* StartTask - sets date_started timestamp
* UpdateTaskStatus (deprecated use task->update())
* CreateTask (deprecated use Task::create(...))
* DeleteTask (deprecated use task->delete())

### Task Owners ###
Models that will be task owners must implement `Sirs\Tasks\Interfaces\Taskowner`.  This is easily accomplished by using the ```Sirs\Tasks\TaskOwnerTraits``` trait on the model and giving the model a fullName method that returns the full name of the owner.  TaskOwnerTraits gives the model a hasMany relationship to tasks.

You can optionally define a Eloquent Resource for task owner in the configuration:
```
...
"owner_resources" => [
  User::class => MyUserResource::class
]
...
```

#### Task management screens ####
Version 2.3 introduces basic task management screens that allow users view tasks and update tasks statuses and due dates outside of task related forms.  

These screens are at <project>/tasks/admin

#### Default task form ####
As of Version 2.3 there is now default task form.  

The form asks whether the task is complete, and allows the user to set a status or leave the task pending if it is not complete.

The route <project>/tasks/<id> will direct the user to the default form for the task.  To direct users to custom forms/screens/surveys when they go to <project>/tasks/<id> see the documentation for the tasks.route middleware below.

### Task REST API ###
REST API Controllers for tasks, types, and statuses.


### Who do I talk to? ###
* TJ Ward - jward3@email.unc.edu