<?php

namespace Sirs\Tasks\Tests\Fixtures;

use Illuminate\Database\Eloquent\Model;
use Sirs\Tasks\Interfaces\TaskOwner;

class User extends Model implements TaskOwner
{
    use \Sirs\Tasks\TaskOwnerTraits;
    
    protected $table = 'users';

    protected $fillable = [
        'name',
        'email',
        'password',
    ];
}
