<?php

namespace Sirs\Tasks\Tests\Feature\Controllers\RestTaskController;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Sirs\Tasks\TaskStatus;
use Sirs\Tasks\TaskType;
use Sirs\Tasks\Task;
use Sirs\Tasks\Tests\Fixtures\User;
use Sirs\Tasks\Tests\TestCase;

class StoreTest extends TestCase
{
	protected function setup(): void
	{
		parent::setup();

		// config()->set('tasks.apiPrefix', 'api');

		$this->loadLaravelMigrations([
            '--database' => 'mysql'
        ]);

		$this->taskStatus = TaskStatus::create(['name'=>'fake']);
		$this->taskType = TaskType::create(['name'=>'name']);
		$this->user = User::create([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => Hash::make('password')
        ]);
	}

	/** 
	 * @test 
	 * @group controllers
	 */
	public function a_user_can_see_a_task()
	{
		$task = Task::create([
			'task_type_id' => $this->taskType->id,
            'owner_type' => User::class,
            'owner_id' => $this->user->id,
            'task_status_id' => $this->taskStatus->id,
            'date_due' => '2021-01-01 00:00:00',
		]);

        $this->assertDatabaseHas('tasks', [
        	'id' => $task->id,
            'task_type_id' => $this->taskType->id,
            'owner_type' => User::class,
            'owner_id' => $this->user->id,
            'task_status_id' => $this->taskStatus->id,
            'date_due' => '2021-01-01 00:00:00',
        ]);
	}

	/** 
	 * @test 
	 * @group controllers
	 * fix this test and make it actually test things
	 */
	public function a_user_can_store_a_task()
	{
		$response = $this->post(route('tasks_api.tasks.store', [
            'task_type_id' => $this->taskType->id,
            'owner_type' => 'Sirs\Tasks\Tests\Fixtures\User',
            'owner_id' => $this->user->id,
            'task_status_id' => $this->taskStatus->id,
            'date_due' => '2021-01-01',
        ]))->assertStatus(201);

        $this->assertDatabaseHas('tasks', [
            'task_type_id' => $this->taskType->id,
            'owner_type' => 'Sirs\Tasks\Tests\Fixtures\User',
            'owner_id' => $this->user->id,
            'task_status_id' => $this->taskStatus->id,
            'date_due' => '2021-01-01',
        ]);
	}
}
