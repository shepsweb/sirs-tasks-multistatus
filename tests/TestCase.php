<?php 

namespace Sirs\Tasks\Tests;

use Orchestra\Testbench\TestCase as OrchestraTestCase;
use Sirs\Tasks\TasksServiceProvider;

class TestCase extends OrchestraTestCase
{
	protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate', ['--database' => 'mysql']);
        $this->loadMigrationsFrom(__DIR__ .'/../src/database/migrations');
        $this->withFactories(__DIR__.'/../src/database/factories');
    }

    /**
	 * Get package providers.
	 *
	 * @param  \Illuminate\Foundation\Application  $app
	 *
	 * @return array
	 */
	protected function getPackageProviders($app)
	{
	    return [
	        TasksServiceProvider::class,
	    ];
	}

	/**
	 * Define environment setup.
	 *
	 * @param  \Illuminate\Foundation\Application  $app
	 * @return void
	 */
	protected function defineEnvironment($app)
	{
	    // Setup default database to use sqlite :memory:
	    $app['config']->set('database.default', 'mysql');
	    $app['config']->set('database.connections.mysql', [
	        'driver' => 'mysql',
            'host' => '192.168.10.10',
            'database' => 'sirs-tasks-multistatus-test',
            'username' => 'homestead',
            'password' => 'secret',
	    ]);
	    // $app['config']->set('database.connections.testbench', [
	    //     'driver'   => 'sqlite',
	    //     'database' => ':memory:',
	    //     'prefix'   => '',
	    // ]);
	    $app['config']->set('sluggable', [
            'source' => null,
            'maxLength' => null,
            'maxLengthKeepWords' => true,
            'method' => null,
            'separator' => '-',
            'unique' => true,
            'uniqueSuffix' => null,
            'includeTrashed' => false,
            'reserved' => null,
            'onUpdate' => false,
        ]);

        $app['config']->set('logging.channels.single', [
            'driver' => 'single',
            'path' => realpath(__DIR__.'/storage/logs/laravel.log'),
            'level' => 'debug',
        ]);
	}
}
