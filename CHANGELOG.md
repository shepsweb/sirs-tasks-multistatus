# Change Log

## 10.0.0 - 2017-01-04
* fix namespacing in factory and seeder classes
* publish seeder classes to `database/seeders` (previously published to `database/seeds`)

## 2.4.0 - 2017-01-04
* Add support for TaskTypeWorkflowStrategies.  Developer can now define task event handling logic for a given task type in a single file.
* Fire task events TaskAutoCanceled, TaskMissed, TaskFailed, etc. based on Task model events.

## 2.3.12 - 2016-12-09
* Added getPastDueAttribute to Task.  returns true if the tasks date_due < now and status is pending (1);

## 2.3.10 - 2016-08-15
* Fixed bug that resulted in unknown index error when asking for $task->data when none set.  Now returns null if no data.

## 2.3.9 - 2016-08-08
* RedirectTasks middleware will pass query params from the original task request url to the redirect url.

## 2.3.8 - 2016-08-08
* Disambiguating orderBy field in Task::getSiblings() method.

## 2.3.7 - 2016-07-13
* Added TaskChildrenCreated event.  Fired when task children are created during the Sirs\Tasks\Commands\CreateTaskChildren job.

## 2.3.1 - 2.3.6
* Minor bug fixes to default task management UI.

## 2.3.0 - 2016-06-28
* Added task management screens with basic task management features (set status and date due).
* Added default task form for tasks that do not have custom routing via the task.router middleware
  * You will need to add the following to your tasks config to make these items work:
    ```
    'ui'=>[
      'chrome'=>'app'
    ],
    ```
  * You will also have to do a global search and replace due to a renamed task route:
    `route('taskShow’,` should be replaced with `route('tasks.show’,`
