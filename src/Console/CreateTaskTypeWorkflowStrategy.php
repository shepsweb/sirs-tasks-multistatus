<?php

namespace Sirs\Tasks\Console;

use File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Sirs\Surveys\Documents\SurveyDocument;
use Sirs\Tasks\Interfaces\TaskType;

class CreateTaskTypeWorkflowStrategy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:workflow 
                            {task_slug : TaskType slug or \'all\' to create workflows for all task types}
                            {--replace : Replace an existing file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create TaskTypeWorkflowStrategy class for task of type';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $taskSlug = $this->argument('task_slug');
        $taskTypes = collect([]);
        if ($taskSlug == 'all') {
            $taskTypes = class_taskType()::all();
        } else {
            $taskTypes = class_taskType()::where('slug', $taskSlug)->get();
            if ($taskTypes->count() < 1) {
                $this->error('Could not find task type with slug '.$this->argument('task_slug').'.');
                $typeSlugs = class_taskType()::all()->pluck('slug')->toArray();
                $this->info("Task types for this project: \n\t".implode("\n\t", $typeSlugs));
                return;
            }
        }

        $taskTypes->each(function ($type) {
            $typeClassName = $type->getSafeWorkflowClassName();
            $test = \Artisan::call('make:test', [
                'name' => 'Tasks/'.$typeClassName.'Test',
                '--unit' => true
            ]);
            $this->info('created test for '.$typeClassName);
            $this->createClass($type);
        });
    }

    protected function createClass(TaskType $taskType)
    {
        $typeClassName = $taskType->getSafeWorkflowClassName();
        $classFileName = $typeClassName.'.php';

        $stub = file_get_contents(__DIR__.'/stubs/TaskTypeWorkflowStrategy.stub');
        $classContent = preg_replace('/{DummyType}/', $typeClassName, $stub);

        if (!file_exists(app_path('Tasks'))) {
            mkdir(app_path('Tasks'));
        }

        $classFile = app_path('Tasks/'.$classFileName);
        if (!file_exists($classFile)) {
            file_put_contents($classFile, $classContent);
            $this->info('Created class at '.$classFile);
        } elseif ($this->confirm("There's already a class for this task type.\n Do you want to replace the existing file? [y|N]")) {
            file_put_contents($classFile, $classContent);
            $this->info('Replaced '.$classFile);
        } else {
            $this->info('Left existing class in place.');
        }
    }
}
