@extends(config('tasks.ui.chrome'))
@section('content')
    <h2>Task Manager</h2>
    <hr>
    <div id="task-manager">
        <task-manager></task-manager>
    </div>
@endsection
@push('scripts')
    <script src="/task_manager_js"></script>
@endpush