@extends(config('tasks.ui.chrome'))

@section('content')
<form method="POST" name="default-task-form">
    {{csrf_field()}}
    {{method_field('PUT')}}

    <div class="panel panel-default w-75" style="width: 75%; margin: auto">
        <div class="panel-heading">
            <h4>{{$task->owner->name}} - {{$task->taskType->name}}</h4>
        </div>
        <div class="panel-body">
            <div class="form-group question-block">
                <div class="question-text">
                    {{$message ?? 'Select a new status for this task:'}}
                </div>
                <div class="row">
                    <div class="question-answers col-sm-9">
                        <div class="btn-group" role="group" data-toggle="buttons">
                            <select name="task_status_id" class="form-control">
                                <option value="">Select...</option>
                                @foreach ($taskStatuses as $status)
                                    <option value="{{$status->id}}">
                                        {{$status->name}}@if($status->is_final)*@endif
                                        @if ($status->id == $task->task_status_id)
                                            (current status)
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                       </div>
                   </div>
                   <div class="col-sm-3">
                    @if ( isset($errors) && $errors->has('task_status_id') )
                    <div class="error-block">
                        <ul class="error-list list-unstyled">
                            @foreach( $errors->get('task_status_id') as $error )
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
        <input class="btn btn-primary" 
            type="submit" 
            name="nav" 
            value="Update Task" 
            id="update-btn" 
        />
    </div>
</div>
</form>
@endsection