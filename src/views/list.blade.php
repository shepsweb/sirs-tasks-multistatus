@extends(config('tasks.ui.chrome'))
@section('content')
<h3>Tasks
    <form name="task-search" method="{{route('tasks.admin.index')}}" class="pull-right">
        <div class="form-group form-inline">
            <label for="search_text"><small>Search</small></label>
            <input type="text" class="form-control" id="search_text" name="search_text" placeholder="task id, task name, owner id" value="{{$search_text}}"></input>
            <input type="submit" value="Go" class="btn btn-default"></input>
            <span class="glyphicon glyphicon-question-sign" title="What can I search?" style="font-size: .55em; vertical-align: super;"
                data-placement="left" 
                data-toggle="popover"  
                data-content="You can search by Task id, name, status or the task owner's id. Owner name is not supported due to technology limitations."></span>
        </div>
    </form>
</h3>
<table class="table table-striped">
    <thead>
        <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Owner (id)</td>
            <td>Status</td>
            <td>Started</td>
            <td>Due</td>
            <td>Updated</td>
            <td colspan="2" class="text-center">Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($tasks as $idx => $task)
        <tr class="task-row{{($task->deleted_at) ? ' deleted' : ''}}" data-task-id="{{$task->id}}">
            <td>{{$task->id}}</td>
            <td>{{$task->taskType->name ?? ''}}</td>
            <td>{{$task->owner->name}} ({{$task->owner->id}})</td>
            <td>{{$task->taskStatus->name ?? ''}}</td>
            <td>{{($task->date_started) ? $task->date_started->format('m/d/Y') : ''}}</td>
            <td>{{$task->date_due->format('m/d/Y')}}</td>
            <td>{{$task->updated_at->format('m/d/Y')}}</td>
            <td class="text-right">
                @if(!$task->deleted_at)
                    <a href="{{route('tasks.show', [$task->id])}}" class="btn btn-sm btn-primary">
                        {{($task->date_started) ? 'Continue' : 'Get Started'}}
                    </a>
                    <a href="{{route('tasks.admin.edit', [$task->id])}}"class="btn btn-sm btn-secondary">
                        edit
                    </a>
                    <form class="delete-task-form" 
                        style="display: inline-block;" 
                        name="delete-task-{{$task->id}}" 
                        action="{{route('tasks.admin.destroy', [$task->id])}}"
                        method="POST"
                        data-task-name="{{$task->id.', '.$task->taskType->name.' for '.$task->owner->name}}">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <button type="submit" class="btn btn-danger btn-sm">
                            delete
                        </button>
                    </form>
                @else
                    <span class="text-danger no-decoration"><strong>Deleted</strong></span>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<hr>
{!! $tasks->render() !!}

@endsection
@section('styles')
<style>
    .deleted {
        text-decoration: line-through;
        color: #aaa;
    }
    span.no-decoration{
        text-decoration: none;
    }
</style>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        // make table rows clickable
        $('.task-row').not('.deleted').css({'cursor': 'pointer'})
            .hover(function(evt){
                $(this).children().addClass('bg-info');
            }, function(evt){
                $(this).children().removeClass('bg-info');
            })
            .on('click', function(evt){
                window.location = '/tasks/admin/'+$(this).attr('data-task-id');
            });
        // delete confirmation
        $('.delete-task-form').on('submit', function(evt){
            return confirm('You are about to delete the task #'+$(this).attr('data-task-name')+".\n You can't undo this action. Do you to continue?");
        });
         
        // search info pop-over
        $(function () {
          $('[data-toggle="popover"]').popover()
        })
    })
</script>
@endsection