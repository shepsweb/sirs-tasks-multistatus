@extends(config('tasks.ui.chrome'))
@section('content')
    <div class="nav-breadcrumb">
      <ul>
        <li><a href="{{route('tasks.admin.show', [$task->id])}}">Tasks</a></li>
        <li><a href="{{route('tasks.admin.show', $task->id)}}">{{$task->taskType->name}} for {{$task->owner->name}} ({{$task->id}})</a></li>
        <li>Edit</li>
      </ul>
    </div>
    <h3>
        {{$task->taskType->name}}
        <a href="{{route('tasks.admin.show', [$task->id])}}" class="btn btn-default pull-right">View</a>
    </h3>
    <form name="task-edit" method="POST" action="{{route('tasks.admin.update', [$task->id])}}">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <dl class="dl-horizontal">
            <div class="row form-group">
                <div class="col-sm-2">Owner</div>
                <div class="col-sm-6">{{$task->owner->name}}</div>
            </div>

            <div class="row form-group">
                <div class="col-sm-2">Status</div>
                <div class="col-sm-6">
                    <select name="task_status_id" class="form-control">
                        <option value="">Select...</option>
                        @foreach($statuses as $status)
                            <option value="{{$status->id}}"
                                {{($task->taskStatus->id == $status->id) ? 'selected' : ''}}>
                                {{$status->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
               <div class="col-sm-3">
                    @if ( isset($errors) && $errors->has('task_status_id') )
                    <div class="error-block">
                        <ul class="error-list list-unstyled">
                            @foreach( $errors->get('task_status_id') as $error )
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-2">Date Created</div>
                <div class="col-sm-6">
                    <input type="text" name="created_at" class="form-control sirs_datepicker" value="{{ $task->created_at->format('m/d/Y') }}" disabled />
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-2">Date Due</div>
                <div class="col-sm-6">
                    <input type="text" name="date_due" class="form-control sirs_datepicker" value="{{ $task->date_due->format('m/d/Y') }}" />
                </div>
               <div class="col-sm-3">
                    @if ( isset($errors) && $errors->has('date_due') )
                    <div class="error-block">
                        <ul class="error-list list-unstyled">
                            @foreach( $errors->get('date_due') as $error )
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>

            <div class="row form-group">
                <div class="col-sm-2">Date Updated</div>
                <div class="col-sm-6">
                    <input type="text" name="updated_at" class="form-control sirs_datepicker" value="{{ $task->updated_at->format('m/d/Y') }}" disabled />
                </div>
            </div>

            @if($task->data)
                <div class="row form-group">
                    <div class="col-sm-2">Data</div>
                    <div class="col-sm-6"><pre>{{ json_encode($task->data) }}</pre></div>
                </div>
            @endif
        </dl>
        <div>
            <input type="submit" value="Save" class="btn btn-primary"></input>
            <input type="reset" value="Reset" class="btn btn-default"></input>
        </div>
    </form>
@endsection