@extends(config('tasks.ui.chrome'))
@section('content')
    <div class="nav-breadcrumb">
      <ul>
        <li><a href="{{route('tasks.admin.index')}}">Tasks</a></li>
        <li>{{$task->taskType->name}} for {{$task->owner->name}} ({{$task->id}})</li>
    </ul>
    </div>
    <h3>
        {{$task->taskType->name}} for {{$task->owner->name}} ({{$task->id}})
    </h3>
    <div class="row row-spacious">
        <div class="col-sm-2"><strong>Owner</strong></div>
        <div class="col-sm-10">{{$task->owner->name}}</div>
    </div>

    <div class="row row-spacious">
        <div class="col-sm-2"><strong>Status</strong></div>
        <div class="col-sm-10">{{$task->taskStatus->name}}</div>
    </div>

    <div class="row row-spacious">
        <div class="col-sm-2"><strong>Date Created</strong></div>
        <div class="col-sm-10">{{ $task->created_at->format('m/d/Y') }}</div>
    </div>

    <div class="row row-spacious">
        <div class="col-sm-2"><strong>Date Due</strong></div>
        <div class="col-sm-10">{{ $task->date_due->format('m/d/Y') }}</div>
    </div>

    <div class="row row-spacious">
        <div class="col-sm-2"><strong>Date Updated</strong></div>
        <div class="col-sm-10">{{ $task->updated_at->format('m/d/Y') }}</div>
    </div>

    @if($task->data)
    <div class="row row-spacious">
        <div class="col-sm-2"><strong>Data</strong></div>
        <div class="col-sm-7">
            <br />
            <table class="table table-striped table-bordered">
                <thead>
                    <th style="width: 20%">key</th>
                    <th>value</th>
                </thead>
                <tbody>
                    @foreach($task->data as $key => $val)
                    <tr><td>{{$key}}</td><td>{{$val}}</td></tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif

    <hr>

    <div>
        <a href="{{route('tasks.admin.edit', [$task->id])}}" class="btn btn-default">Edit</a>
        <form class="delete-task-form" 
            style="display: inline-block;" 
            name="delete-task-{{$task->id}}" 
            action="{{route('tasks.admin.destroy', [$task->id])}}"
            method="POST"
            data-task-name="{{$task->id.', '.$task->taskType->name.' for '.$task->owner->name}}">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <button type="submit" class="btn btn-default">
                Delete
            </button>
        </form>
        @if ($task->task_status_id == 1) 
        <a href="{{route('tasks.show', [$task->id])}}" class="btn btn-default">{{($task->date_started) ? 'Continue Task' : 'Start Task'}}</a>
        @endif
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        // delete confirmation
        $('.delete-task-form').on('submit', function(evt){
            return confirm('You are about to delete the task #'+$(this).attr('data-task-name')+".\n You can't undo this action. Do you to continue?");
        });
    })
</script>
@endsection