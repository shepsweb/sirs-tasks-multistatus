<?php

namespace Sirs\Tasks\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskDefaultUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'task_status_id' => 'required|exists:task_statuses,id'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'This field is required'
        ];
    }
}
