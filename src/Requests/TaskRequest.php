<?php 
namespace Sirs\Tasks\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Validator;


class TaskRequest extends FormRequest {

	/**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function __construct()
  {
    /**
     * Check to see if a polymorphic relation id exists
     */
    Validator::extend('polyexists', function($attribute, $value, $parameters)
    {
      if( !class_exists($parameters[0]) ){
        return false;
      }
      if( count(forward_static_call([$parameters[0], 'find'], [$value])) > 0 ){
        return true;
      }
      return false;

    });

    Validator::replacer('polyexists', function($message, $attribute, $rule, $parameters)
    {
      $message = str_replace(':model', $parameters[0], $message);
      return str_replace(':fieldname', str_replace('_id', '', $attribute), $message);
    });

    /**
     * Check to see if a submitted polymorphic relation type exists and implements an interface
     */
    Validator::extend('validpolytype', function($attribute, $value, $parameters)
    {
      if( class_exists($value) ){
        if( isset($params[0]) ){
          if( in_array($parameters[0], class_implements($value)) ){
            return true;
          }
        }else{
          return true;
        }
      }
      return false;
    });
      
    Validator::replacer('validpolytype', function($message, $attribute, $rule, $parameters)
    {
      return str_replace(':interface', $parameters[0], $message);
    });
  }

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'task_type_id'=>'required|exists:task_types,id',
			'owner_type'=>'required|validpolytype:Sirs\Tasks\Interfaces\TaskOwner',
			'owner_id'=>'required|polyexists:'.$this->input('owner_type'),
			'date_due'=>'required|date'
		];
	}

	/**
	 * return a response with errors formatted for api
	 *
	 * @param array $errors array of errors
	 * @return void
	 **/
	public function response(array $errors){
		$formattedErrors = [];
		foreach( $errors as $field => $errorMessage ){
			$formattedErrors[] = ['field'=>$field, 'message'=>$errorMessage];
		}
		return Response::json([
			'errors'=>$errors
		], 403);
	}
}
