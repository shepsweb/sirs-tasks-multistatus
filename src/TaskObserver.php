<?php
namespace Sirs\Tasks;

use Auth;
use Event;
use Sirs\Tasks\AppointmnentType;
use Sirs\Tasks\Events\TaskAutoCanceled;
use Sirs\Tasks\Events\TaskCanceled;
use Sirs\Tasks\Events\TaskCompleted;
use Sirs\Tasks\Events\TaskCreated;
use Sirs\Tasks\Events\TaskDataUpdated;
use Sirs\Tasks\Events\TaskDeleted;
use Sirs\Tasks\Events\TaskFailed;
use Sirs\Tasks\Events\TaskMissed;
use Sirs\Tasks\Events\TaskReOpened;
use Sirs\Tasks\Events\TaskStarted;
use Sirs\Tasks\Events\TaskStatusUpdated;
use Sirs\Tasks\Events\TaskStatusWasUpdated;
use Sirs\Tasks\Events\TaskWasCreated;
use Sirs\Tasks\Interfaces\Task;
use Sirs\Tasks\Interfaces\TaskStatus;

/**
 * participant model observer
 *
 * @package Change
 * @author
 **/
class TaskObserver
{
    use \Illuminate\Foundation\Bus\DispatchesJobs;

    public $eventsDispatcher;

    public function __construct()
    {
    }

    public function saving(Task $task)
    {
        // get the current user and set the updated_by_type and updated_by_id
        $user = Auth::user(); // this probably shouldn't be here b/c, theoretically some other model could update a task.
        if ($user) {
            $task->updated_by_type = get_class($user);
            $task->updated_by_id = $user->id;
        }

        if (! $task->owner_type) {
            throw new InvalidOwnerType(null);
        }
    }

    public function created(Task $task)
    {
        Event::dispatch(new TaskWasCreated($task));
        Event::dispatch(new TaskCreated($task));
    }

    public function updated(Task $task)
    {
        if ($task->isDirty('task_status_id')) {
            Event::dispatch(new TaskStatusWasUpdated($task));
            Event::dispatch(new TaskStatusUpdated($task));
            switch ($task->fresh()->taskStatus->means) {
                case 'pending': // changed back to pending
                    if (class_taskStatus()::find($task->getOriginal('task_status_id'))->means != 'pending') {
                        Event::dispatch(new TaskReOpened($task));
                    }
                    break;
                case 'completed': // changed to complete
                    Event::dispatch(new TaskCompleted($task));
                    break;
                case 'canceled': // changed to canceled
                    Event::dispatch(new TaskCanceled($task));
                    break;
                case 'auto-canceled': // changed to autocanceled
                    Event::dispatch(new TaskAutoCanceled($task));
                    break;
                default:
                    break;
            }
        }
        
        if ($task->isDirty('date_started') && !is_null($task->date_started)) {
            Event::dispatch(new TaskStarted($task));
        }

        if ($task->isDirty('data')) {
            Event::dispatch(new TaskDataUpdated($task));
        }
    }

    public function deleted(Task $task)
    {
        Event::dispatch(new TaskDeleted($task));
    }
} // END class ParticipantObserver
