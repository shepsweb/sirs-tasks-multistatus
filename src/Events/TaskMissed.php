<?php namespace Sirs\Tasks\Events;

use Sirs\Tasks\Events\TaskEvent;
use Sirs\Tasks\Interfaces\Task;

use Illuminate\Queue\SerializesModels;

class TaskMissed extends TaskStatusWasUpdated {

  use SerializesModels;

  public $task;

  /**
   * Create a new event instance.
   *
   * @param  Task $task task that status was updated on
   * @return void
   */
  public function __construct(Task $task)
  {
    $this->task = $task;
  }

}
