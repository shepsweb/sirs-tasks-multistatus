<?php namespace Sirs\Tasks\Events;

use Sirs\Tasks\Events\TaskEvent;
use Sirs\Tasks\Interfaces\Task;

use Illuminate\Queue\SerializesModels;

class TaskStatusWasUpdated extends TaskStatusUpdated {
}
