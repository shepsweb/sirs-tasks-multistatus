<?php namespace Sirs\Tasks\Events;

use Sirs\Tasks\Events\TaskEvent;
use Sirs\Tasks\Interfaces\Task;
use Illuminate\Queue\SerializesModels;

class TaskWasCreated extends TaskCreated {
}
