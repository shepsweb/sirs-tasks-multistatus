<?php
$routeGroup = (config('tasks.routeGroup')) ? config('tasks.routeGroup') : [];
Route::group($routeGroup, function () {
    Route::group(['prefix'=>config('tasks.apiPrefix')], function () {
        Route::resource('tasks', '\Sirs\Tasks\Controllers\RestTaskController', ['as'=>'tasks_api']);
        Route::patch('tasks/{id}/complete', ['as'=>'completeTask', 'uses'=>'\Sirs\Tasks\Controllers\RestTaskController@complete']);
        Route::patch('tasks/{id}/fail', ['as'=>'failTask', 'uses'=>'\Sirs\Tasks\Controllers\RestTaskController@fail']);
        
        Route::resource('task-types', '\Sirs\Tasks\Controllers\Api\TaskTypeController', ['as'=>'taskTypes', 'only' => ['index', 'show']]);

        Route::get('task-statuses', ['as'=>'taskStatuses', 'uses'=>'\Sirs\Tasks\Controllers\RestTaskController@statuses']);
    });

    Route::group(['as'=>'tasks.'], function () {
        Route::resource('tasks/admin', '\Sirs\Tasks\Controllers\TaskController');
    });
    Route::get('tasks/manager', function () {
        return view('tasks::task-manager');
    })->name('task-manager');

    Route::get('tasks/{id}', ['as'=>'tasks.show', 'uses'=>'\Sirs\Tasks\Controllers\TaskController@defaultShow']);
    Route::put('tasks/{id}', ['as'=>'tasks.update', 'uses'=>'\Sirs\Tasks\Controllers\TaskController@defaultUpdate']);

    // Route::get('workflow/{id}', ['as'=>'showWorkflow', 'uses'=>'\Sirs\Tasks\Controllers\TaskController@showWorkflow']);
    // Route::post('workflow/{id}', ['as'=>'updateWorkflow', 'uses'=>'\Sirs\Tasks\Controllers\TaskController@saveWorkflow']);
});
Route::get('task_manager_js', function () {
    $jsContent = file_get_contents(__DIR__.'/../dist/task_manager.js');
    return \Response::make($jsContent, 200)
            ->header('Content-Type', 'application/javascript');
});
