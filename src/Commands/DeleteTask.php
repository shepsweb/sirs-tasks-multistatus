<?php namespace Sirs\Tasks\Commands;

use Sirs\Tasks\Commands\Command;

use Sirs\Tasks\Interfaces\Task;


class DeleteTask extends Command {

  var $task;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct(Task $task)
  {
    $this->task = $task;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function handle()
  {
    $this->task->delete();
  }

}