<?php 
namespace Sirs\Tasks\Commands;

use Sirs\Tasks\Commands\Command;

class CreateTask extends Command {

  var $task_type_id;
  var $owner_type;
  var $owner_id;
  var $date_due;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct($task_type_id, $owner_type, $owner_id, $date_due)
  {
    $this->task_type_id = $task_type_id;
    $this->owner_type = $owner_type;
    $this->owner_id = $owner_id;
    $this->date_due = $date_due;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function handle()
  {
    class_task()::create([
      'task_type_id' => $this->task_type_id,
      'owner_type' => $this->owner_type,
      'owner_id' => $this->owner_id,
      'date_due' => $this->date_due,
    ]);
  }

}
