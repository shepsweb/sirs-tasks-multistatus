<?php namespace Sirs\Tasks\Commands;

use Bus;
use Sirs\Tasks\Commands\Command;
use Sirs\Tasks\Commands\UpdateTaskStatus;
use Sirs\Tasks\Interfaces\Task;

class CompleteTask extends Command {

  var $task;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct(Task $task)
  {
    $this->task = $task;
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function handle()
  {
    Bus::dispatch(new UpdateTaskStatus($this->task, class_taskStatus()::findByName('completed')));
  }

}