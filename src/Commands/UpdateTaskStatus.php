<?php namespace Sirs\Tasks\Commands;

use Sirs\Tasks\Commands\Command;
use Sirs\Tasks\Interfaces\Task;
use Sirs\Tasks\Interfaces\TaskStatus;
use Sirs\Tasks\Commands\StartTask;
use Bus;

class UpdateTaskStatus extends Command {

  var $task;
  var $newStatus;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct(Task $task, TaskStatus $newStatus)
  {
    $this->task = $task;
    $this->newStatus = $newStatus;
  }

  /**
   * update the task's status and save
   *
   * @return void
   * @author 
   **/
  public function handle()
  {
    if( is_null($this->task->date_started) ){
      Bus::dispatch(new StartTask($this->task));
    }
    $this->task->task_status_id = $this->newStatus->id;
    $this->task->save();
  }

}
