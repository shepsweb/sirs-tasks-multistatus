<?php namespace Sirs\Tasks\Commands;

use Sirs\Tasks\Commands\Command;
use Sirs\Tasks\Interfaces\Task;
use Carbon\Carbon;
use Bus;

class StartTask extends Command {

  var $task;
  var $dateStarted;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct(Task $task, Carbon $date_started = null)
  {
    $this->task = $task;
    $this->dateStarted = ($date_started) ? $date_started : new Carbon();
  }

  /**
   * undocumented function
   *
   * @return void
   * @author 
   **/
  public function handle()
  {
    $this->task->startTask($this->dateStarted);
  }

}