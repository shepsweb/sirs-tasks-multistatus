<?php
namespace Sirs\Tasks;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Sirs\Tasks\Interfaces\TaskStatus as TaskStatusInterface;

class TaskStatus extends Model implements TaskStatusInterface
{
    use Sluggable;
    use SluggableScopeHelpers;
  
    protected $table = 'task_statuses';
    protected $fillable = [
        'name',
        'is_final',
        'means'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source'=>'name'
            ]
        ];
    }

    public function taskTypes()
    {
        return $this->belongsToMany(TaskType::class);
    }

    public static function findByName($name)
    {
        return static::where('name', '=', $name)
                ->get()
                ->first();
    }

    /**
     * Scopes query to is_final == 1
     */
    public function scopeFinal($query)
    {
        return $query->where('is_final', 1);
    }
}
