<?php

namespace Sirs\Tasks\Middleware;

use Closure;

class RedirectTasks
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $path = (string)$request->path();
        if( preg_match('/^tasks\/\d*$/', $path) ){
            // get the path details
            list($route, $id) = preg_split('/\//', $path);

            // get redirectUrl from taskRedirect function in the config
            if( config('tasks.router') && !$request->bypassRouter ){
                $routerClass = config('tasks.router');
                $router = new $routerClass();

                $redirectUrl = $router->resolve(class_task()::findOrFail($id));

                if( $redirectUrl ){
                    $url = parse_url($redirectUrl);

                    $params = [];
                    if (isset($url['query'])) {
                        parse_str($url['query'], $params);
                    }

                    if ($request->query()) {
                        foreach ($request->query() as $key => $value) {
                            $params[$key] = $value;
                        }
                    }
                    
                    $url['path'] = preg_replace('/^\//', '', $url['path']);
                    $url = $url['scheme'].'://'.$url['host'].'/'.$url['path'];
                    if($params){
                        $url .= '?'.http_build_query($params);
                    }
                    return redirect($url);
                }
            }
        }
        return $next($request);
    }

    public function buildUrl($task)
    {

    }
}
