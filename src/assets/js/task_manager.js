import Vue from 'vue';
import TaskManager from './components/Tasks/TaskManager/TaskManager'

// initialize Vue app
if (document.getElementById('task-manager')) {
    const taskManager = new Vue({
        el: '#task-manager',
        components: {
            TaskManager
        }
    })
}
