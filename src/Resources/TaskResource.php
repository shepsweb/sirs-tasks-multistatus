<?php

namespace Sirs\Tasks\Resources;

use Sirs\Tasks\Resources\OwnerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        $data['name'] = $this->name;
        $data['date_started'] = $this->date_due->format('Y-m-d');
        $data['date_due'] = $this->date_due->format('Y-m-d');
        $data['owner'] = new OwnerResource($this->whenLoaded('owner'));
        $data['task_type'] = new TaskTypeResource($this->whenLoaded('taskType'));
        $data['task_status'] = new TaskStatusResource($this->whenLoaded('taskStatus'));
        $data['child_order'] = ($this->relationLoaded('taskType')) ? $this->taskType->child_order : null;
        $data['children'] = TaskResource::collection($this->whenLoaded('children'));
        if (
            $this->relationLoaded('owner')
            && isset(config('tasks.owner_resources')[get_class($this->owner)])
        ) {
            $resourceClass = config('tasks.owner_resources')[get_class($this->owner)];
            $data['owner'] = new $resourceClass($this->whenLoaded('owner'));
        }
        
        return $data;
    }
}
