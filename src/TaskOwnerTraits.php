<?php 
namespace Sirs\Tasks;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

trait TaskOwnerTraits
{
    public function tasks()
    {
        return $this->morphMany(class_task(), 'owner');
    }

    protected function tasksWithStatus($taskStatusId)
    {
        return $this->tasks()->where('task_status_id', '=', $taskStatusId);
    }

    protected function getTasksWithStatus($taskStatusId)
    {
        return $this->tasksWithStatus($taskStatusId)->get();
    }

    public function getPendingTasks()
    {
        return $this->tasksWithStatus(class_taskStatus()::findByName('Pending')->id)->orWhere('task_status_id', '=', null)->get();
    }

    public function getCompletedTasks()
    {
        return $this->getTasksWithStatus(class_taskStatus()::findByName('Complete')->id);
    }

    public function getMissedTasks()
    {
        return $this->getTasksWithStatus(class_taskStatus()::findByName('Missed')->id);
    }

    public function getFailedTasks()
    {
        return $this->getTasksWithStatus(class_taskStatus()::findByName('Failed')->id);
    }

    public function getCanceledTasks()
    {
        return $this->getTasksWithStatus(class_taskStatus()::findByName('Cancelled')->id);
    }

    public function getAutocanceledTasks()
    {
        return $this->getTasksWithStatus(class_taskStatus()::findByName('Autocancelled')->id);
    }

    public function getUrlAttribute()
    {
        $classParts = explode('\\', get_class($this));
        return '/'.Str::plural(strtolower(end($classParts))).'/'.$this->id;
    }

    public function getNameAttribute()
    {
        if (\Schema::hasColumn($this->getTable(), 'name')) {
            return $this->attributes['name'];
        }
        if (\Schema::hasColumn($this->getTable(), 'first_name') && \Schema::hasColumn($this->getTable(), 'last_name')) {
            return $this->attributes['first_name'].' '.$this->attributes['last_name'];
        }
    }

    /**
     * Cancels all pending tasks for this task owner
     *
     * @return Collection collection of canceled tasks
     */
    public function cancelPendingTasks()
    {
        $canceledTasks = new Collection();
        foreach ($this->getPendingTasks() as $task) {
            $task->task_status_id = class_taskStatus()::findByName('Cancelled')->id;
            $task->save();
            $canceledTasks->push($task);
        }
        return $canceledTasks;
    }
}
