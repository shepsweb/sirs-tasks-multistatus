<?php
namespace Sirs\Tasks;

use Config;
use DB;
use Event;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Sirs\Tasks\Console\CreateTaskTypeWorkflowStrategy;
use Sirs\Tasks\Events\TaskEvent;
use Sirs\Tasks\Events\TaskStatusWasUpdated;
use Sirs\Tasks\Events\TaskWasCreated;
use Sirs\Tasks\Handlers\Events\TaskStatusWasUpdatedHandler;

class TasksServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the Tasks services.
     * Adds listener for Task::created, Task::updated, and TaskStatusWasUpdated Events
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->bindInterfaces();

        $this->registerListeners();

        // load the views
        $this->loadViewsFrom(__DIR__.'/views', 'tasks');

        // Publish the config files
        $this->publish();

        // register the routes
        require __DIR__.'/routes.php';
        
        // bind the task model to the task routes
        $router->model('api/tasks', class_task());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            CreateTaskTypeWorkflowStrategy::class,
        ]);
    }

    protected function bindInterfaces()
    {
        $this->app->bind(
            \Sirs\Tasks\Task::class,
            config('tasks.bindings.models.Task', \Sirs\Tasks\Task::class)
        );
        $this->app->bind(
            \Sirs\Tasks\TaskType::class,
            config('tasks.bindings.models.TaskType', \Sirs\Tasks\TaskType::class)
        );
        $this->app->bind(
            \Sirs\Tasks\TaskStatus::class,
            config('tasks.bindings.models.TaskStatus', \Sirs\Tasks\TaskStatus::class)
        );


        $this->app->bind(
            \Sirs\Tasks\Interfaces\Task::class,
            config('tasks.bindings.models.Task', \Sirs\Tasks\Task::class)
        );
        $this->app->bind(
            \Sirs\Tasks\Interfaces\TaskType::class,
            config('tasks.bindings.models.TaskType', \Sirs\Tasks\TaskType::class)
        );
        $this->app->bind(
            \Sirs\Tasks\Interfaces\TaskStatus::class,
            config('tasks.bindings.models.TaskStatus', \Sirs\Tasks\TaskStatus::class)
        );
    }

    protected function publish()
    {
        // Publish the triggers class
        $this->publishes([
            __DIR__.'/config/config.php' => config_path('tasks.php'),
        ], 'config');

        // Publish the migrations
        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('/migrations')
        ], 'task_migrations');

        // Publish the seeds
        $this->publishes([
            __DIR__.'/database/seeds/' => database_path('/seeders')
        ], 'task_seeds');

        // Publish the factories
        $this->publishes([
            __DIR__.'/database/factories/' => database_path('/factories')
        ], 'task_seeds');

        $this->publishes([
            __DIR__.'/TaskRouterDefault.php' => app_path('TaskRouter.php')
        ], 'router');
    }

    protected function registerListeners()
    {
        // register observers:
        class_task()::observe(new TaskObserver);

        $this->registerWorkflowEvents();
        
        Event::listen('Sirs\Tasks\Events\TaskCreated', 'Sirs\Tasks\Handlers\Events\TaskWasCreatedHandler');
        Event::listen('Sirs\Tasks\Events\TaskStarted', 'Sirs\Tasks\Handlers\Events\StartParentTask');
    }

    protected function registerWorkflowEvents()
    {
        // Register RunTaskWorkflow listener
        // There has to be a better way to do this, but I can't find it.
        Event::listen('Sirs\Tasks\Events\TaskAutoCanceled', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskCanceled', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskChildrenCreated', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskCompleted', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskCreated', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskDataUpdated', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskDeleted', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskFailed', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskMissed', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskReOpened', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskStarted', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
        Event::listen('Sirs\Tasks\Events\TaskStatusUpdated', 'Sirs\Tasks\Handlers\Events\RunTaskWorkflow');
    }
}
