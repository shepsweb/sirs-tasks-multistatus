<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class TaskTypeSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Model::unguard();
    $taskTypes = collect(config('tasks.types'));
    if( $taskTypes->count() > 0 ){
      foreach( $taskTypes as $type ){
        class_taskType()::create($type);
      }
    }
  }
}