<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TaskTypesStatusesSeeder extends Seeder
{

  /**
   * Run the database seeds.
   *
   * @return void
   */
    public function run()
    {
        Model::unguard();

        if (config('tasks.type_status_definition_path') === null) {
            throw new \Exception('You must specify type_status_definiteion_path in config/tasks.php with the location of your types/statuses definition');
        }
        if (! file_exists(config('tasks.type_status_definition_path'))) {
            throw new \Exception('tasks.type_status_definition_path '.config('tasks.type_status_definition_path').' does not exist');
        }
        $types = json_decode(file_get_contents(config('tasks.type_status_definition_path')), true);

        foreach ($types as $type) {
            $statuses = collect($type['statuses'])->map(function ($status) {
                return class_taskStatus()::firstOrCreate($status);
            });
            unset($type['statuses']);
            $type = class_taskType()::create($type);
            $type->taskStatuses()->attach($statuses->pluck('id'));
        }
    }
}
