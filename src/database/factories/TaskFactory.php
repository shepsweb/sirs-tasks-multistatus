<?php

namespace Database\Factories;

use App\Task;
use Illuminate\Database\Eloquent\Factories\Factory;
 
class TaskFactory extends Factory
{
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $part = App\Participant::all()->random();
        $taskType = class_taskType()::inRandomOrder()->first();

        return [
            'task_type_id'=>$taskType->id,
            'owner_type'=>get_class($part),
            'owner_id'=>$part->id,
            'date_due'=>$this->faker->dateTimeBetween('-10 days', '+14 days'),
            'task_status_id'=>1,
        ];
    }
}
