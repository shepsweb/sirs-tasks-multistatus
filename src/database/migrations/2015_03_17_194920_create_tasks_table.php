<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('task_type_id')->unsigned();
			$table->string('owner_type');
			$table->integer('owner_id')->unsigned();
      		$table->json('data')->nullable();
			$table->integer('task_status_id')->unsigned()->default(1);
			$table->integer('parent_task_id')->unsigned()->nullable();
			$table->integer('triggered_by_task_id')->unsigned()->nullable();
			$table->integer('current_survey_step')->unsigned()->nullable();
			$table->date('date_started')->nullable();
			$table->date('date_due');
			$table->timestamps();
			$table->string('updated_by_type')->nullable();
			$table->string('updated_by_id')->nullable();
			$table->softDeletes();

			$table->index('task_type_id');
			$table->index(['owner_type', 'owner_id']);
			$table->index(['updated_by_type', 'updated_by_id']);
		});


		Schema::table('tasks', function(Blueprint $table){
			$table->foreign('parent_task_id')->references('id')->on('tasks')->onDelete('cascade');
		});

		Schema::table('tasks', function(Blueprint $table){
			$table->foreign('triggered_by_task_id')->references('id')->on('tasks')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}
