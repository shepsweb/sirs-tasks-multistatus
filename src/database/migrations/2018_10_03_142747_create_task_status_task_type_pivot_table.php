<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskStatusTaskTypePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_status_task_type', function (Blueprint $table) {
            $table->integer('task_status_id')->unsigned()->index();
            $table->foreign('task_status_id')->references('id')->on('task_statuses')->onDelete('cascade');
            $table->integer('task_type_id')->unsigned()->index();
            $table->foreign('task_type_id')->references('id')->on('task_types')->onDelete('cascade');
            $table->primary(['task_status_id', 'task_type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_status_task_type');
    }
}
