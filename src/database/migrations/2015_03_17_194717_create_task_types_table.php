<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_types', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->string('slug')->unique();
			$table->string('description')->nullable();
			$table->boolean('has_children')->nullable();
			$table->integer('child_order')->nullable();
			$table->integer('display_order')->default(99);
			$table->integer('parent_task_type_id')->unsigned()->nullable();
			$table->timestamps();
		});

		Schema::table('task_types', function(Blueprint $table)
		{
			$table->foreign('parent_task_type_id')->references('id')->on('task_types')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('task_types');
	}

}
