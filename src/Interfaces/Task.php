<?php
Namespace Sirs\Tasks\Interfaces;

/**
 * Defines interface all task owners must adhear to.
 *
 * @package default
 * @author 
 **/
interface Task
{
    public function owner();

    /**
     * gets child tasks
     *
     * @return collection
     * @author tj ward
     **/
    public function children();
    public function parent();

    public function taskType();

    public function taskStatus();

    public function updatedBy();

    public function appointments();

  // Accessors and Mutators
  
    public function getDates();

    /**
     * checks to see if $value is in config('tasks.ownerTypes') if not found throws exception
     *
     * @param string $value value of owner_type
     * @throws InvalidOwnerId 
     * @throws InvalidOwnerType
     * @return void
     * @author 
     **/
    public function setOwnerTypeAttribute($value);

    /**
     * checks to see if the owner id exists in the owner_type's table
     *
     * @param int $value value of owner_id
     * @throws InvalidOwnerId [description]
     * @return void
     * @author 
     **/
    public function setOwnerIdAttribute($value);

    /**
     * gets a carbon instance for the date due
     *
     * @return Carbon
     * @author TJ Ward
     **/
    public function getDateDueAttribute($value);

  // Scopes
    public function scopeStatus($query, $status_id);

    public function scopePending($query);

    public function scopeNotChild($query);

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function scopeType($query, $typeIdOrSlug);

  // Domain methods
  
    /**
     * Adds the current date as start_date to the task and saves it
     *
     * @return void
     * @author tj ward
     **/
    public function startTask (\Carbon\Carbon $date_started = null);

    /**
     * Adds the current date as start_date to the task and saves it
     *
     * @return void
     * @author tj ward
     **/
    public function saveData( $data );
    public function siblingsQuery();
    public function getSiblings();

    public function getNameAttribute();
} // END interface OwnerInterface

?>