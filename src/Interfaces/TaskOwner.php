<?php
namespace Sirs\Tasks\Interfaces;

/**
 * Defines interface all task owners must adhear to.
 *
 * @package default
 * @author
 **/
interface TaskOwner
{
    public function tasks();
    public function getPendingTasks();
    public function getCompletedTasks();
    public function getMissedTasks();
    public function getFailedTasks();
    public function getCanceledTasks();
    public function cancelPendingTasks();
    public function getUrlAttribute();
    public function getNameAttribute();
} // END interface OwnerInterface
