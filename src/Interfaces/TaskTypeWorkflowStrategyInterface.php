<?php 

namespace Sirs\Tasks\Interfaces;

use Sirs\Tasks\Events\TaskEvent;
use \Sirs\Tasks\Interfaces\Task;

/**
 * Defines interface for task workflow strategies.
 *
 * @package Sirs\Tasks
 **/
interface TaskTypeWorkflowStrategyInterface
{
    /*
     * Constructor
     * @param Sirs\Tasks\Interfaces\Task $task
     * @param Sirs\Tasks\Events\TaskEvent $event
     */
    public function __construct(Task $task, TaskEvent $event);

    /*
     * runs the appropriate method based on $this->task and $this->event
     */ 
    public function run();

    /*
     * Run when event is TaskAutoCanceled
     */ 
    public function autoCanceled();

    /*
     * Run when event is TaskCanceled
     */ 
    public function canceled();

    /*
     * Run when event is TaskChildrenCreated
     */ 
    public function childrenCreated();

    /*
     * Run when event is TaskCompleted
     */ 
    public function completed();

    /*
     * Run when event is TaskCreated
     */ 
    public function created();

    /*
     * Run when event is TaskDataUpdated
     */ 
    public function dataUpdated();

    /*
     * Run when event is TaskDeleted
     */ 
    public function deleted();

    /*
     * Run when event is TaskFailed
     */ 
    public function failed();

    /*
     * Run when event is TaskMissed
     */ 
    public function missed();

    /*
     * Run when event is TaskReOpened
     */ 
    public function reOpened();

    /*
     * Run when event is TaskStarted
     */ 
    public function started();

    /*
     * Run when event is TaskStatusUpdated
     */ 
    public function statusUpdated();

} // END interface TaskWorkflowStrategyInterface