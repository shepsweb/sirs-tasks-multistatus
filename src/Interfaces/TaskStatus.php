<?php
Namespace Sirs\Tasks\Interfaces;

/**
 * Defines interface all task statuses must adhear to.
 *
 * @package default
 * @author 
 **/
interface TaskStatus
{
    public function sluggable();

    public static function findByName($name);
}