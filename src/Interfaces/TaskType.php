<?php
namespace Sirs\Tasks\Interfaces;

/**
 * Defines interface all task types must adhear to.
 *
 * @package default
 * @author
 **/
interface TaskType
{
    public function sluggable();

    public function children();

    public function parent();

    public function siblings();

    public function tasks();

    public function siblingsQuery();

    public function getSiblings();
    
    public function getWorkflowClass();

    public function getSafeWorkflowClassName();

    public static function findBySlug($slug);
}
