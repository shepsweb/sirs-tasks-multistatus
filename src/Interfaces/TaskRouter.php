<?php
namespace Sirs\Tasks\Interfaces;

/**
 * TaskRouters get the url for a task based on it's type and project specific rules
 *
 * @package default
 * @author 
 **/
interface TaskRouter
{
  /**
   * Gets the url if custom routing 
   *
   * @return mixed url or null if default
   **/
  public function resolve(Task $task);
} // END interface TaskRouter