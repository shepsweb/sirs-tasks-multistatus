<?php
namespace App;

use Sirs\Tasks\Interfaces\TaskRouter as TaskRouterInterface;
use Sirs\Tasks\Interfaces\Task;

class TaskRouter implements TaskRouterInterface
{
  public function resolve(Task $task){
      $taskToAction = config('tasks.taskToAction');
      if( !isset($taskToAction[$task->taskType->slug]) ) return null;

      list($type, $identifier) = preg_split('/:/', $taskToAction[$task->taskType->slug]);

      switch ($type) {
        case 'survey':
          return url('app-participant/'.$task->owner->id.'/survey/'.$identifier);
        case 'appointment':
          $url = 'appointments/create?appointment_type_id='.$identifier.'&attendee_id='.$task->owner->identifier;
          $forTask = null;
          switch ($identifier) {
            case 2:
              // $forTask = $task->owner->tasks->where('task_type_id', 1)->first();
              break;              
            default:
              break;
          }
          $url = ( $forTask ) ? $url.'&task_id='.$forTask->identifier : $url;
          return url($url);
        case 'url':
          return url($identifier);
        default:
          return null;
          break;
      }
  }
}