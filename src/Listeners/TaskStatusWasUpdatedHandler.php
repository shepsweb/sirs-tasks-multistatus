<?php 
namespace Sirs\Tasks\Listeners;

use Sirs\Tasks\Events\TaskStatusWasUpdated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class TaskStatusWasUpdatedHandler {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  Events  $event
	 * @return void
	 */
	public function handle(TaskStatusWasUpdated $event)
	{
		$task = $event->task;

    $lastStatusName = class_taskStatus()::find($task->getOriginal()['task_status_id'])->name;
    $newStatusName = class_taskStatus()::find($task->task_status_id)->name;

    $taskTypeSlug = Str::camel($task->taskType->slug);

    $anyToNewMethod = Str::camel($taskTypeSlug.'AnyTo'.$newStatusName);
    $oldToAnyMethod = Str::camel($taskTypeSlug.$lastStatusName.'ToAny');
    $oldToNewMethod = Str::camel($taskTypeSlug.$lastStatusName.'To'.$newStatusName);

    // run the old to any trigger
    if(method_exists(config('tasks.triggerClass'), $anyToNewMethod)){
      forward_static_call_array([config('tasks.triggerClass'), $oldToAnyMethod], [$task]);
    }
    // run the old to any trigger
    if(method_exists(config('tasks.triggerClass'), $oldToAnyMethod)){
      forward_static_call_array([config('tasks.triggerClass'), $oldToAnyMethod], [$task]);
    }
    // run the old to new trigger
    if( method_exists(config('tasks.triggerClass'), $oldToNewMethod) ){
      forward_static_call_array([config('tasks.triggerClass'), $oldToNewMethod], [$task]);
    }
	}

}
