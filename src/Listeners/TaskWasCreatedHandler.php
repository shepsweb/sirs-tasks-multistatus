<?php 
namespace Sirs\Tasks\Listeners;

use Sirs\Tasks\Events\TaskWasCreated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class TaskWasCreatedHandler {

  /**
   * Create the event handler.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  Events  $event
   * @return void
   */
  public function handle(TaskWasCreated $event)
  {
    $task = $event->task;
    if( $task->taskType->has_children ){
      foreach( $task->taskType->children as $childTaskType ){
        class_task()::create([
                      'task_type_id'=>$childTaskType->id,
                      'owner_type'=>$task->owner_type,
                      'owner_id'=>$task->owner_id,
                      'parent_task_id'=>$task->id,
                      'date_due'=>$task->date_due,
                      'triggered_by_task_id'=>$task->triggered_by_task_id
                     ]);
      }
    }
  }

}
