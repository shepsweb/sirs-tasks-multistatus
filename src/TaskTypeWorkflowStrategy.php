<?php

namespace Sirs\Tasks;

use Sirs\Tasks\Events\TaskEvent;
use Sirs\Tasks\Interfaces\TaskTypeWorkflowStrategyInterface;
use Sirs\Tasks\Interfaces\Task;

/**
 * undocumented class
 *
 * @package default
 * @author 
 **/
class TaskTypeWorkflowStrategy implements TaskTypeWorkflowStrategyInterface
{

    protected $task;
    protected $event;


    public function __construct(Task $task, TaskEvent $event){
        $this->task = $task;
        $this->event = $event;
    }

    public function run()
    {
        $reflection = new \ReflectionClass(get_class($this->event));

        $method = lcfirst(substr($reflection->getShortName(), 4));
        
        if ( method_exists($this, $method) ) {
            $this->$method($this->task);
        }
        
    }

    public function autoCanceled()
    {
    }

    public function canceled()
    {
    }

    public function childrenCreated()
    {
    }

    public function completed()
    {
    }

    public function created()
    {
    }

    public function dataUpdated()
    {
    }

    public function deleted()
    {
    }

    public function failed()
    {
    }

    public function missed()
    {
    }

    public function reOpened()
    {
    }

    public function started()
    {
    }

    public function statusUpdated()
    {
    }

} // END class TaskTypeWorkflowStrategy