<?php 
namespace Sirs\Tasks\Handlers\Events;

use App\TaskTriggers;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Bus;
use Sirs\Tasks\Commands\CreateTaskChildren;
use Sirs\Tasks\Events\TaskChildrenCreated;
use Sirs\Tasks\Events\TaskCreated;

class TaskWasCreatedHandler {

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Events  $event
     * @return void
     */
    public function handle(TaskCreated $event)
    {
        Bus::dispatch(new CreateTaskChildren($event->task));
    }

}
