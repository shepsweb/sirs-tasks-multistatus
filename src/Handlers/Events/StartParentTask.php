<?php 
namespace Sirs\Tasks\Handlers\Events;

use Sirs\Tasks\Events\TaskStarted;
use Sirs\Tasks\Commands\StartTask;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Bus;

class StartParentTask {

  /**
   * Create the event handler.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  Events  $event
   * @return void
   */
  public function handle(TaskStarted $event)
  {
    $task = $event->task;

    if( $task->parent ){
      Bus::dispatch(new StartTask($task->parent));
    }
  }

}
