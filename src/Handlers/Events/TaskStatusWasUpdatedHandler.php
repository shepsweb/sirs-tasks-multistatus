<?php 
namespace Sirs\Tasks\Handlers\Events;

use App\TaskTriggers;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;
use Sirs\Tasks\Events\TaskStatusUpdated;
use Sirs\Tasks\Interfaces\TaskStatus;

class TaskStatusWasUpdatedHandler {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  Events  $event
	 * @return void
	 */
	public function handle(TaskStatusUpdated $event)
	{
		$task = $event->task;

    $lastStatusName = class_taskStatus()::find($task->getOriginal()['task_status_id'])->name;
    $newStatusName = class_taskStatus()::find($task->task_status_id)->name;

    $taskTypeSlug = Str::camel($task->taskType->slug);

    $anyToNewMethod = Str::camel($taskTypeSlug.'AnyTo'.$newStatusName);
    $oldToAnyMethod = Str::camel($taskTypeSlug.$lastStatusName.'ToAny');
    $oldToNewMethod = Str::camel($taskTypeSlug.$lastStatusName.'To'.$newStatusName);

    // run the old to any trigger
    if(method_exists(config('tasks.triggerClass'), $anyToNewMethod)){
      forward_static_call_array([config('tasks.triggerClass'), $oldToAnyMethod], [$task]);
    }
    // run the old to any trigger
    if(method_exists(config('tasks.triggerClass'), $oldToAnyMethod)){
      forward_static_call_array([config('tasks.triggerClass'), $oldToAnyMethod], [$task]);
    }
    // run the old to new trigger
    if( method_exists(config('tasks.triggerClass'), $oldToNewMethod) ){
      forward_static_call_array([config('tasks.triggerClass'), $oldToNewMethod], [$task]);
    }
	}

}
