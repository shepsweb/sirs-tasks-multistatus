<?php
namespace Sirs\Tasks\Handlers\Events;

use Bus;
use Sirs\Tasks\Events\TaskEvent;
use Sirs\Tasks\Commands\StartTask;
use Sirs\Tasks\Events\TaskCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class RunTaskWorkflow
{

  /**
   * Create the event handler.
   *
   * @return void
   */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Events  $event
     * @return void
     */
    public function handle(TaskEvent $event)
    {
        $task = $event->task->fresh();
        
        $workflowClassName = $task->taskType->getWorkflowClass();
        if (class_exists($workflowClassName)) {
            $workflow = new $workflowClassName($task, $event);
            $workflow->run();
        }
    }
}
