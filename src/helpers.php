<?php

if (! function_exists('class_task')) {
	function class_task() {
		return config('tasks.bindings.models.Task', \Sirs\Tasks\Task::class);
	}
}

if (! function_exists('class_taskType')) {
	function class_taskType() {
		return config('tasks.bindings.models.TaskType', \Sirs\Tasks\TaskType::class);
	}
}

if (! function_exists('class_taskStatus')) {
	function class_taskStatus() {
		return config('tasks.bindings.models.TaskStatus', \Sirs\Tasks\TaskStatus::class);
	}	
}

?>