<?php

namespace Sirs\Tasks\Controllers;

use Bus;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Redirect;
use Response;
use Sirs\Tasks\Commands\CompleteTask;
use Sirs\Tasks\Commands\FailTask;
use Sirs\Tasks\Commands\StartTask;
use Sirs\Tasks\Requests\TaskRequest;
use Sirs\Tasks\Resources\TaskResource;
use Sirs\Tasks\Resources\TaskStatusResource;
use Sirs\Tasks\Resources\TaskTypeResource;

class RestTaskController extends Controller
{
    protected $validFilters = [
        'task_status_id',
        'owner_type',
        'owner_id',
        'parent_task_id',
        'task_type_id',
        'not_task_type_id',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // This should probably go in a repository, but not right now.
        // handle any filters we get from the query
        // $query = class_task()::with('taskType', 'taskStatus', 'owner');
        $with = ($request->query('with')) ? explode(',', $request->query('with')) : [];
        $with[] = 'owner';

        $query = class_task()::with($with)->notChild();

        // if user has a site_id and participants table has site_id field,
        // then limit tasks in task list to shared site
        if (\Auth::user()->site_id && Schema::hasColumn('participants', 'site_id')) {
            $query->join('participants', function ($join) {
                $join->on('tasks.owner_id', '=', 'participants.id')
                    ->where('tasks.owner_type', '=', 'App\Participant')
                    ->where('participants.site_id', '=', \Auth::user()->site_id);
            })->select(\DB::raw('tasks.*'));
        }

        foreach ($request->all() as $field=>$value) {
            $not = false;
            if (substr($field, 0, 4) == 'not_') {
                $field = substr($field, 4);
                $not = true;
            }

            if (in_array($field, $this->validFilters)) {
                if ($not) {
                    if (is_array($value)) {
                        $query->whereNotIn($field, $value);
                    } else {
                        $query->where($field, '!=', $value);
                    }
                } else {
                    if (is_array($value)) {
                        $query->whereIn($field, $value);
                    } else {
                        $query->where($field, '=', $value);
                    }
                }
            }

            if ($field == 'start_date') {
                $query->where('updated_at', '>=', Carbon::parse($value)->startOfDay());
            }

            if ($field == 'end_date') {
                $query->where('updated_at', '<=', Carbon::parse($value)->endOfDay());
            }

            if ($field == 'due_start') {
                $query->where('date_due', '>=', Carbon::parse($value)->startOfDay());
            }

            if ($field == 'due_end') {
                $query->where('date_due', '<=', Carbon::parse($value)->endOfDay());
            }
        }
        $count = $query->count();

        if ($orderBy = $request->orderBy) {
            $query->orderBy($orderBy['attr'], $orderBy['dir']);
        }

        if ($limit = $request->query('limit')) {
            $query->take($limit);
        }
        if ($offset = $request->query('offset')) {
            $query->skip($offset);
        }

        return TaskResource::collection($query->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return Redirect::action('TaskController@index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(TaskRequest $request)
    {
        try {
            $input = $request->all();
            $task = class_task()::create($input);

            return new TaskResource($task);
        } catch (Exception $e) {
            return Response::json([  'errors'=> [ ['message' => $e->getMessage()] ] ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return Response
     */
    public function show($id)
    {
        $task = class_task()::findOrFail($id);
        $task->load('owner', 'children');

        return new TaskResource($task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return Response
     */
    public function edit($id)
    {
        return Redirect::action('TaskController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $task = class_task()::findOrFail($id);
        Bus::dispatch(new StartTask($task));
        $task->update($request->all());

        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return Response
     */
    public function destroy($id)
    {
        $task = class_task()::findOrFail($id);
        $task->delete();

        return Response::json('success', 200);
        // return Redirect::action('RestTaskController@index');
    }

    public function complete($id)
    {
        $task = class_task()::findOrFail($id);
        Bus::dispatch(new CompleteTask($task));

        return $this->show($id);
    }

    public function fail($id)
    {
        $task = class_task()::findOrFail($id);
        Bus::dispatch(new FailTask($task));

        return $this->show($id);
    }

    public function types(Request $request)
    {
        return TaskTypeResource::collection(class_taskType()::all());
    }

    public function statuses()
    {
        return TaskStatusResource::collection(class_taskStatus()::all());
    }
}
