<?php

namespace Sirs\Tasks\Controllers;

use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Sirs\Tasks\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Sirs\Tasks\Requests\TaskDefaultUpdate;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_text = $request->search_text;
        $taskQuery = class_task()::with('owner', 'taskStatus', 'taskType');
        if ($search_text) {
            $taskQuery->where('tasks.id', '=', $search_text)->orWhere('owner_id', '=', $search_text);
            $taskQuery->orWhereHas('taskType', function ($query) use ($search_text) {
                $query->where('name', 'LIKE', '%'.$search_text.'%');
            });
            $taskQuery->orWhereHas('taskStatus', function ($query) use ($search_text) {
                $query->where('name', 'LIKE', '%'.$search_text.'%');
            });
        }
        $tasks = $taskQuery->paginate(10);
        $tasks->appends(['search_text'=>$search_text]);
        return response()->view('tasks::list', ['tasks'=>$tasks, 'search_text'=>$search_text]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!preg_match('/tasks\/admin/', URL::previous())) {
            session()->put('prev_page', URL::previous());
        }
        $task = class_task()::findOrFail($id);
        return response()->view('tasks::detail', ['task'=>$task]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = class_task()::findOrFail($id);
        $statuses = $task->taskType->taskStatuses;
        return response()->view('tasks::edit', ['task'=>$task, 'statuses'=>$statuses]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'task_status_id'=>'required|exists:task_statuses,id',
            'date_due'=>'required'
        ]);
        if ($validator->fails()) {
            return redirect(route('tasks.admin.edit', [$id]))->withErrors($validator)->withInput();
        }
        $task = class_task()::findOrFail($id);
        $attributes = $request->except('_token', '_method');
        $attributes['date_due'] = strtotime($attributes['date_due']);
        $task->update($attributes);
        return redirect()->route('tasks.admin.show', [$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = class_task()::findOrFail($id);
        $task->delete();
        if (session()->has('prev_page')) {
            return redirect(session()->pull('prev_page'));
        } else {
            return redirect()->route('tasks.admin.index');
        }
    }

    public function defaultShow(Request $request, $id)
    {
        $task = class_task()::findOrFail($id);
        $taskStatuses = $task->taskType->taskStatuses;

        // set ther previous location in the session so we can redirect to it after form is submitted.
        session()->put('prev_page', URL::previous());

        return response()->view('tasks::default_task_form', compact('task', 'taskStatuses'));
    }

    public function defaultUpdate(TaskDefaultUpdate $request, $id)
    {
        $task=class_task()::findOrFail($id);
        $task->update(['task_status_id'=>$request->task_status_id]);
        
        $prev_page = session()->pull('prev_page', '/');

        return redirect($prev_page);
    }
}
