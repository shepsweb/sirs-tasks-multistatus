<?php

namespace Sirs\Tasks\Controllers\Api;

use Illuminate\Http\Request;

class TaskTypeController extends Controller
{
    private $dummy;

    public function __construct()
    {
        $class = class_taskType();
        $this->dummy = new $class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = class_taskType()::query();
        $query = $this->queryFromRequest($query, $request);
        return $query->get();
    }

    private function queryFromRequest($query, Request $request)
    {
        if ($request->has('select')) {
            $selectParams = (is_array($request->select)) ? $request->with : explode(',', $request->select);
            // dd($selectParams);
            $selectable = array_filter($selectParams, function ($param) {
                return $this->modelHasAttribute($param);
            });

            $query->select($selectable);
        }

        if ($request->with) {
            $with = (is_array($request->with)) ? implode(',', $request->with) : $request->with;
            return $query->with($with);
        }


        if ($request->where) {
            $whereParams = (is_array($request->where)) ? $request->with : explode(',', $request->where);
            foreach ($whereParams as $attr => $value) {
                if ($this->modelHasAttribute($attr)) {
                    $query->where($attr, $value);
                }
            }
        }

        if ($request->orderBy && $this->modelHasAttribute($attr)) {
            $dir = 'asc';
            if (strtolower($request->orderByDir) == 'desc') {
                $dir = 'desc';
            }
            $query->orderBy($request->orderBy, $dir);
        }

        return $query;
    }

    private function modelHasAttribute($attr)
    {
        return in_array($attr, $this->dummy->getFillable());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return class_taskType()::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
