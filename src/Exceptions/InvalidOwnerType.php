<?php 
namespace Sirs\Tasks\Exceptions;

/**
 * Class defining invalid owner type exception
 *
 * @package default
 * @author
 **/
class InvalidOwnerType extends \Exception
{
    public $givenOwnerType = null;

    public function __construct($givenOwnerType, $message = null, $code = 0, Exception $previous = null)
    {
        $this->givenOwnerType = $givenOwnerType ?? 'null';
        $this->message = ($message) ? $message : 'Invalid task owner type. '.$this->givenOwnerType.' does not implement Sirs\Tasks\Interfaces\TaskOwner.';
    }
} // END class InvalidOwnerTypeExceiption
