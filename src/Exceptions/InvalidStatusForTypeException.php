<?php 
namespace Sirs\Tasks\Exceptions;

use Sirs\Tasks\Interfaces\TaskType;

/**
 * Class defining invalid owner type exception
 *
 * @package default
 * @author
 **/
class InvalidStatusForTypeException extends \Exception
{
    private $taskType;

    public function __construct(TaskType $taskType, $invalidStatusId, $message = null, $code = 0, Exception $previous = null)
    {
        $this->taskType = $taskType;
        if (is_null($message)) {
            $message = 'Invalid task status ('.$invalidStatusId.') for task with type '.$this->taskType->name
                        .'.  Expects one of the following task statuses'
                        ."\n".$taskType->taskStatuses
                            ->map(function ($item) {
                                $final = ($item->is_final) ? 'final': 'not final';
                                return $item->id.':'.$item->name.' ('.$final.')';
                            })
                            ->implode("\n");
        }
        $this->message = $message;
    }
} // END class InvalidOwnerTypeExceiption
