<?php 
namespace Sirs\Tasks\Exceptions;

/**
 * Class defining invalid owner type exception
 *
 * @package default
 * @author 
 **/
class InvalidOwnerId extends \Exception
{

  public $givenOwnerId = null;
  public $ownerType = null;

  public function __construct($givenOwnerId, $ownerType, $message = null, $code = 0, Exception $previous = null){
    $this->givenOwnerId = $givenOwnerId;
    $this->ownerType = $ownerType;
    $this->message = ($message) ? $message : 'Invalid task owner. '.$this->ownerType.' with id '.$this->givenOwnerId.' does not exist';
  }
} // END class InvalidOwnerTypeExceiption
?>
